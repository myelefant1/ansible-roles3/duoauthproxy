## Duo auth proxy

This role installs and configures Duo auth proxy

Tested on debian 10.

Reference: https://duo.com/docs/authproxy-reference

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|------------------------------------------|
| duoauthproxy_configuration  | dict     | no        |                 | configuration for the proxy              |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/duoauthproxy.git
  scm: git
  version: v1.5
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: duoauthproxy
      duoauthproxy_configuration:
        ad_client:
          host: 1.2.3.4
          service_account_username: duoservice
          service_account_password: password1
          search_dn: DC=example,DC=com
          security_group_dn: CN=DuoVPNUsers,OU=Groups,DC=example,DC=com
        ldap_server_auto:
          client: ad_client
          ikey: DIXXXXXXXXXXXXXXXXXX
          skey: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          api_host: api-XXXXXXXX.duosecurity.com
          failmode: secure
          exempt_primary_bind: false
          exempt_ou_1: CN=ldaplookup,dc=acme,dc=org
          ssl_key_path: ldap_server.key
          ssl_cert_path: ldap_server.pem
```

## Tests

[tests/tests_duoauthproxy](tests/tests_duoauthproxy)
